from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

import build_model as build_model
import checkpoint
import configuration.global_config as config
import data_loader as data_loader
import logging
import os
import torch
import torch.nn as nn
import utils
import utility_tools.image_utilities as image_utilities


def test(args, inferenceModel, loss):
    """
        Run inference with model
        Inputs: args (input arguments), inferenceModel (neural network model),
                costFunction (objective function for training)
        Outputs: -
    """
    if isinstance(inferenceModel, nn.Module):
        print("Is instance of nn.Module")
        inferenceModel.eval()
    args.batchSizeIter = 1
    args.lenDataset = len(args.dataloader)


    for i, data in enumerate(args.dataloader):
        args.iter = i
        signal = data[0]
        args.filename = data[1][0]
        if len(data)>2:
            label = data[2][0]
        if len(data)>3:
            mask = data[3][0]
        
        # print("FILENAME:", args.filename)
        
        estimate = inferenceModel.forward(signal, args)

        if not args.resultsPath:
            logging.warning('Please specify path to save intermediary results - resultsPath')
            break
        else:
            data_dict = {}
            data_dict['estimated'] = estimate.detach().numpy()
            if len(data)>2:
                data_dict['label'] = label.detach().numpy()
            if len(data)>3:
                data_dict['mask'] = mask.detach().numpy()[0]
            print("Done! Subject {}".format(i))
            # image_utilities.plot_scatter(data_dict['label'],data_dict['estimated'])
            # image_utilities.imagebrowse_slider(data_dict['estimated'])
            image_utilities.saveItermediateResults(data_dict, args)


def kill_hanging_processes():
    osPID = os.getpid()
    command = 'taskkill /F /PID ' + str(osPID)
    os.system(command)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logging.info('Setting up environment for parameter estimation...')

    configuration = config.GlobalConfig('testing')
    configuration.args.loadCheckpoint = True
    configuration.args.stateName = 'testing'

    if not configuration.args.loadCheckpointPath and not configuration.args.inferenceModel=='MLE':
        logging.warn("Checkpoint path not specified. Please insert a valid checkpoint path")
    else:
        inferenceModel, args = build_model.make(configuration.args)
        logging.info('{} model succesfully built.'.format(args.inferenceModel))

        configuration.args.dataloader = data_loader.create_dataloader(args)[0]
        logging.info('Starting inference...')
        test(args, inferenceModel, list(args.loss.values())[0])

    if not sys.platform == 'darwin':
        kill_hanging_processes()
