from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import os

import datasets.utilities as datasetUtilities
import torch
from torch.utils.data import DataLoader
from torch.utils.data import Dataset


class SampleDatabase(Dataset):
    def __init__(self, args, path):
        self.args = args
        self.args.dataset.set_data_path(path)
        self.args.dataset.index_files()

    def __len__(self):
        return self.args.dataset.get_length()

    def __getitem__(self, idx):
        label, mask = self.args.dataset.get_training_label(idx)
        filename = self.args.dataset.fileName[idx][:-4]
        signal = self.args.dataset.get_training_signal(idx)
        if len(label)==0:
            return signal, filename
        else:
            return signal, filename, label, mask


def create_dataloader(args):
    """
        Function: create_dataloader(config_parameters, run_type, transformation_sequence, args = "")

        Description: Creates the dataloader handler

        Args:
            INPUT: 
                - config_parameters (DICT): A Dictionary containing the configuration parameters for the method. 
                - run_type (STRING): A Dictionary containing the configuration parameters for the method. 
                - transformation_sequence (DICT): A Dictionary containing the configuration parameters for the method. 
                - args (DICT): A Dictionary containing the configuration parameters for the method. 
            OUTPUT: 
                - dataloader (DICT): A dictionary containing the sequence of data transformations to be applied
                                                  during training and validation.
    """

    if args.mode == 'TRAINING':
        training_database_handler = SampleDatabase(args=args, path=args.trainingData)
        trainingDataloader = DataLoader(training_database_handler,
                                batch_size = 1 if args.usePatches else args.batchSize,
                                shuffle = False,
                                num_workers = 0
                                )
        if args.runValidation:
            validation_database_handler = SampleDatabase(args=args, path=args.validationData)
            validationDataloader = DataLoader(validation_database_handler,
                                    batch_size = 1 if args.usePatches else args.batchSize,
                                    shuffle = False,
                                    num_workers = 0
                                    )
            dataloader = [trainingDataloader, validationDataloader]
        else:
            dataloader = [trainingDataloader]

    elif args.mode == 'TESTING':
        testing_database_handler = SampleDatabase(args=args, path=args.testingData)
        testingDataloader = DataLoader(testing_database_handler,
                                batch_size = 1,
                                shuffle = False,
                                num_workers = 0
                                )
        dataloader = [testingDataloader]
    return dataloader