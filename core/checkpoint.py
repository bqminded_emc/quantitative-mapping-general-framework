from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import os
import torch
import logging


def save(args, epoch, model):
    """
        Save trained DL model to checkpoint.
        Inputs: args (console inputs), epoch and model

    """
    path = os.path.join(args.saveCheckpointPath, args.inferenceModel+'_epoch_'+str(epoch) + '.pth')
    torch.save({'epoch': epoch,
                'model_state': model.state_dict(),
                'optimiser_state': args.optimiser.state_dict()}, path)

    logging.info('Model saved to: {}'.format(path))


def load(checkpointPath, model, optimiser):
    """
        Load saved DL model from checkpoint.
        Inputs: args (console inputs), epoch and model
        Outputs: DL model, optimiser checkpoint and epoch
    """
    checkpoint = torch.load(checkpointPath, map_location='cpu')
    model.load_state_dict(checkpoint['model_state_dict'])
    optimiser.load_state_dict(checkpoint['optimizer_state_dict'])
    epoch = checkpoint['epoch']

    logging.info('Model architecture: {}'.format(model))
    return model, optimiser, epoch
