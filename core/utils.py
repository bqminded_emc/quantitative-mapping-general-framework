from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
import logging
import numpy as np
from progress.bar import FillingSquaresBar
import torch

def alternateTrainingState(opt, dataloader):
    while True:
        if len(dataloader)>1:
            yield {'training': dataloader[0], 'batchSize': opt.batchSize}
            yield {'validation': dataloader[1], 'batchSize': 1}
        else:
            yield {'training': dataloader[0], 'batchSize': opt.batchSize}


def batch_iterator(args, batchSize):
    start_index = 0
    while True:
        data = yield
        end_index = args.numberOfPatches if start_index + batchSize > args.numberOfPatches else start_index + batchSize
        batched_data = []
        for dat in data:
            batched_data.append(dat[start_index:end_index])
        yield batched_data, [end_index, start_index]
        if end_index != args.numberOfPatches:
            start_index += batchSize


def ProgressBarWrap(func):
    bar = FillingSquaresBar('Processing', max=1, suffix='%(percent)d%%', loss=10)
    def wrapper(self, loss, args):
        bar.set_max(args.number_of_iterations)
        bar.set_total_sub(args.lenDataset)
        bar.update_ext_par(loss, args.iter)
        bar.next()
        
        if bar.index == args.number_of_iterations:
            bar.finish()
            bar.reset()
    return wrapper
    
