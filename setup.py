import os
from setuptools.command.install import install
from setuptools import setup, find_packages
import sys

class BuildTorchWinCPU(install):
    """Custom build command."""
    def run(self):
        print("\nInstalling PyTorch for Windows - NO GPU SUPPORT\n")
        command = 'pip install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html'
        os.system(command)

        print("\n Updating Matplotlib and installing QT for visualization")
        os.system('pip install --upgrade matplotlib')
        os.system('pip install pyqt5')

class BuildTorchWinGPU(install):
    """Custom build command."""
    def run(self):
        print("\nInstalling PyTorch for Windows - WITH GPU SUPPORT\n")
        command = 'pip install torch==1.6.0 torchvision==0.7.0 -f https://download.pytorch.org/whl/torch_stable.html'
        os.system(command)

        print("\n Updating Matplotlib and installing QT for visualization")
        os.system('pip install --upgrade matplotlib')
        os.system('pip install pyqt5')
        


setup(cmdclass={
        'build_torch_win_cpu': BuildTorchWinCPU,
        'build_torch_win_gpu': BuildTorchWinGPU,
      },
      name='QMRI_framework',
      version='0.1',
      description='Tool for QMRI',
      url='https://bitbucket.org/bqminded_emc/quantitative-mapping-general-framework/',
      author='Emanoel Ribeiro Sabidussi',
      author_email='e.ribeirosabidussi@erasmusmc.nl',
      license='MIT',
      packages=find_packages(include=['.*']),
      install_requires=[
          'h5py>=2.10.0',
          'numpy>=1.19.0',
          'matplotlib>=3.3.0',
          'visdom>=0.1.8.8',
          'scipy>=1.2.1',
          ['torch>=1.3.1'] if sys.platform == 'darwin' else [],
      ],
      zip_safe=False)