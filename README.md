
# How to use this tool

## Installation
Tested **python** versions= 3.6.5 (Windows), 3.6.8 (MacOS) and 3.7.1 (MacOS). Let me know if you installed in with a different python version.

Clone the repository in a local folder:
```shell
$ git clone https://e_sabidussi@bitbucket.org/bqminded_emc/quantitative-mapping-general-framework.git
```

### MacOS
#### Create virtual environment
For all purposes, we will work on a virtual environment, so packages and versions will not conflict with locally available packages. After cloning the repository, navigate to the new folder and run (in CLI):
```shell
$ python -m venv QMRI_fw_env
$ source ./QMRI_fw_env/bin/activate
```

to deactivate the virtual environment, run:
```shell
$ deactivate
```

#### Installing all dependencies
All dependencies are automatically installed when you run
```shell
$ python setup.py install
```
Third-party packages versions are available with

```shell
$ cat requirements.txt
```

### Windows
#### Create virtual environment
For all purposes, we will work on a virtual environment, so packages and versions will not conflict with locally available packages. After cloning the repository, navigate to the new folder and run (in CLI):
```shell
$ py -m venv QMRI_fw_env
$ .\QMRI_fw_env\Scripts\activate
```
*Note that, for Windows, the 64bit version of Python is executed with **py** and not **python**. Trying to install the dependencies with Python32 will generate installation errors.*

To deactivate the virtual environment, run:
```shell
$ .\QMRI_fw_env\Scripts\deactivate.bat
```

#### Installing all dependencies
For Windows systems, we must specify that PyTorch must be installed from a Windows wheel, with or without GPU:
###### With GPU

```shell
$ python setup.py build_torch_win_gpu install
```
###### CPU only

```shell
$ python setup.py build_torch_win_cpu install
```

## Training the network

In the command line, navigate to **./core/**. In this folder there are two files, one for training (**train_model.py**) and one for inference (**estimate.py**). The command to run each follows the same format: (check the **global_config.py** file for all options available)

```shell
$ python train_model.py --task $TASK$ (*relaxometry, qsm, etc...*) -options (from global_config) TRAINING -options (from mode_config) $TASK$ (*RELAXOMETRY, QSM, etc...* - all uppercase) -options (from task_config)
```

## Example: running training
To run the training routine on the example data provided (task: relaxometry with simulated data), you execute the python script as:

```shell
$ python train_model.py --task relaxometry --inferenceModel RIM --configuration ../Mapping_Methods/RIM/resources/configuration_file_rim.txt --loadCheckpoint False TRAINING --trainingData ../example_training_data/Simulated/anatomical_models/training/ --runValidation False -saveCheckpoint False -preLoadData True RELAXOMETRY -signalModel T1 -useSimulatedData True -likelihood Gaussian
```

## Example: running inference
To run inference on the test data provided, you run the python script as:

```shell
python estimate.py --task relaxometry --inferenceModel RIM --configuration ../Mapping_Methods/RIM/resources/configuration_file_rim.txt --loadCheckpoint True -loadCheckpointPath ../Trained_Models/RIM/relaxometry/T1/RIM_T1_6T_36C_31TI_ReLu_24Batch_fixedNoise_T1init_299.pth -usePatches False TESTING --testingData ../example_training_data/In_vivo/T1w/1/rep1/ --resultsPath . RELAXOMETRY -signalModel T1 -useSimulatedData False -likelihood Gaussian
```

# How to contribute by adding new quantitative methods
## File structure
This project is divided into six main folders:

1. **signal_models**:
    This folder contains the structure needed for the definition of signal models (i.e. 3 parameter model for relaxometry). The file **baseSignalModel.py** is an interface class, containing the template of all methods that your signal_model class should implement.

2. **likelihood_models**:
    This folder contains the structure needed for the definition of likelihood functions (i.e. PDF of the acquired data). The file **baseLL.py** is an interface class, containing the template of all methods that your new likelihood model class should implement.

3. **datasets**:
    This folder contains the structure needed for the definition of a task specific dataset (e.g. relaxometry with simulated data). The file **baseDataset.py** is an interface class, containing the template of all methods that your dataset class should implement. This abstraction is usefull for you to create custom datasets or import data in task-specific ways.

4. **configuration**:
    This folder contains global and task-specific configurations. Global configurations (file **global_config.py**) are needed to set up the training/inference routines that are common  to most applications. Task-specific configurations are settings specific for your application. In relaxometry, options such as the inversion times/echo times are set here. Because this framework was built to be as general as possible, not all inference models require the definition of a signal or likelihood models (for example, the ResNet). For that reason, these configurations are also taken as task-specific.

5. **mapping_methods**:
    It contains all mapping methods available within this framework. The base file **baseInferenceMethod.py** is the interface class, containing the template of the methods that must be implemented in a new mapping method.

6. **core**:
    Contains the main rountines for training (**train_model.py**), inference (**estimate.py**), model checkpoint saving/loading (**checkpoint.py**), setting the dataset loader (**data_loader.py**) and the construction of the entire estimation model (**build_model.py**).
    Normally, for the inclusion of new methods and models, the only file to be modified in this folder is the **build_model.py**, in which new methods can be defined.

## IMPLEMENTING A NEW TASK
In our framework, a task is defined by the problem at hand. Normally, creating a new task will mean the cration of a new Signal_Model, definition of configuration options and linking the new models to the pipeline:

#### New signal model:

In the folder **./signal_models/** you will find the abstract method **baseSignalModel.py**. This class should be inherited by all signal models.

Step 1. Create a folder named according to your model;

Step 2. Within that folder, create a file with the same name, but all lower case (e.g. **./signal_models/QSM/qsm.py**);

Step 3. Within the the newly created file, you must define a class for your model. Follow the format in the examples provided (*LookLocker* and *FSE*);

#### Defining task-specific configuration:

In the folder **./configuration/** you will find the file **global_config.py**, which contains all configurations not specific to a given task. Task-specific configurations are defined in separate files, and instanciated in the **global_config.py** file.

Step 1. Create a folder named according to your task (e.g. relaxometry, qsm);

Step 2. Within that folder, create a file named **'task'_config.py**, where 'task' is the same name as your folder.

Step 3. Within the the created file, you must define a class for your model. Follow the format in the examples provided (*relaxometry*). This class should inherit the *GlobalConfig* class

Step 4. Link the created configuration class to the framework. Do this in the **__init__** method of the *GlobalConfig* class (**global_config.py**) under the section **TASK CONFIG**

#### Creating a custom dataset:
TODO

#### Linking new models to the framework:

You must link the newly created model to the framework. You will find where to do this within the file **./core/build_model.py**. The file is divided in sections for each component of the framework: Cost function, Likelihood model, Signal model, Task and Inference Model.
First include a new *elif* condition for the new model under **SIGNAL_MODELS** section. Examples for LookLocker and FSE are provided. The import should reference the class you just created.

Then, under the **TASK** section, include a new *elif* condition for the new task and import the appropriate Dataset (task-specific).





# Compatibility
Tested systems:
1. MacOS 10.14.6
2. MacOS 10.15.3
3. Windows 7 64bits SP1


# Known bugs
- case insensitive systems (e.g. MacOs) might have problems indexing folders with lowercase name. This causes git to pull folders names starting with a capital letter. If you try to run a test, you will receive an import error. If that happens a quick fix is to rename all folders to its lowercase version. To ensure that git is tracking folders with lowercase name, run the command below:

```shell
$ git ls-files
```