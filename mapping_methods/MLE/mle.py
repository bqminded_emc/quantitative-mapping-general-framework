from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

from ..baseInferenceMethod import BaseInferenceMethod
from core.utils import ProgressBarWrap
import json
import logging
import torch
import torch.nn as nn
from torch.autograd import Variable
import utility_tools.image_utilities as image_utilities




class MLE(BaseInferenceMethod, object):
    """
    """

    def __init__(self, configurationFilePath, signalModel, noiseModel):
        super(MLE, self).__init__()
        self.configurationFile = configurationFilePath #Required
        self.signalModel = signalModel
        self.noiseModel = noiseModel
        self.__parseConfigFile__()
        self.args = []
        # self.__buildNetwork__()
        
    def __parseConfigFile__(self):
        """
            Parses all configuration contained in the configuration file
        """
        print(self.configurationFile)
        with open(self.configurationFile) as json_file:  
            config_params = json.load(json_file)
        
        self.config_parameters = {}
        self.config_parameters["number_of_iterations"] = config_params["Configuration-file"]["optimizer-parameters"]["number_of_iterations"]

    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    
    def forward(self, signal, args):
        self.args = args
        self.args.number_of_iterations = self.config_parameters["number_of_iterations"]

        paramMaps = self.signalModel.initializeParameters(signal, self.args)
        self.args.optimiser = torch.optim.Adam(list(paramMaps), lr=self.args.LR, betas=(0.8, 0.899))
        for i in range(self.config_parameters["number_of_iterations"]):
            self.args.optimiser.zero_grad()

            wImages = self.signalModel.generateWeightedImages(paramMaps)
            loss = self.noiseModel.logLikelihood(signal, wImages, self.args.sigmaNoise)
            self.update_bar(loss.item(), self.args)

            loss.backward()
            self.args.optimiser.step()
            estimates = paramMaps


        return torch.stack(estimates)
            
        
