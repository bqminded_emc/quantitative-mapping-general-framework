from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')

from ..baseInferenceMethod import BaseInferenceMethod
from core.utils import ProgressBarWrap
import json
from .rnn import RNN
import torch
import torch.nn as nn
from torch.autograd import Variable



class RIM(BaseInferenceMethod, nn.Module):
    """
        Class Implementing the RIM model.
        Methods:
            - setOpts
                inputs: a Dict containing the key and value for a new configuration setting
            - forward 
                inputs: signal (measured signal); args (options containing, at least args.batchSize and args.device)
                outputs: Estimated parameters
    """

    def __init__(self, configurationFilePath, signalModel, noiseModel):
        super(RIM, self).__init__()
        self.configurationFile = configurationFilePath #Required
        self.signalModel = signalModel
        self.noiseModel = noiseModel
        self.__parseConfigFile__()
        self.__buildNetwork__()

    def __buildNetwork__(self):
        """
            Hidden method that instanciates a version of the RNN module based on the configuration file
        """
        self.rnn = RNN(self.config_parameters["channels_input"], 
                        self.config_parameters["out_channels_l1"],
                        self.config_parameters["out_channels_l2"],
                        self.config_parameters["out_channels_l3"],
                        self.config_parameters["channels_output"],
                        self.config_parameters
                        )

    def __parseConfigFile__(self):
        """
            Parses all configuration contained in the configuration file
        """
        with open(self.configurationFile) as json_file:  
            config_params = json.load(json_file)

        self.config_parameters = {}
        self.config_parameters["channels_input"] = config_params["Configuration-file"]["network-parameters"]["channels_input"]
        self.config_parameters["out_channels_l1"] = config_params["Configuration-file"]["network-parameters"]["out_channels_l1"]
        self.config_parameters["out_channels_l2"] = config_params["Configuration-file"]["network-parameters"]["out_channels_l2"]
        self.config_parameters["out_channels_l3"] = config_params["Configuration-file"]["network-parameters"]["out_channels_l3"]
        self.config_parameters["channels_output"] = config_params["Configuration-file"]["network-parameters"]["channels_output"]
        self.config_parameters["rnn_iterations"] = config_params["Configuration-file"]["network-parameters"]["number_rnn_time_steps"]

    def __initHidden__(self, signal, args):
        """
            Initialises all hidden states in the network
        """
        shape_input = torch.tensor((signal.shape)[2:])
        shape_hs1 = [1, args.batchSizeIter*int(torch.prod(shape_input)), self.config_parameters["out_channels_l1"]]
        shape_hs2 = [1, args.batchSizeIter*int(torch.prod(shape_input)), self.config_parameters["out_channels_l3"]]
        st_1 = Variable(torch.zeros(tuple(shape_hs1)).to(device=args.device))
        st_2 = Variable(torch.zeros(tuple(shape_hs2)).to(device=args.device))
        return [st_1, st_2]

    def __getGradients__(self, signal, maps):
        """
            Compute and return gradients of the Likelihood Function w.r.t. parameter maps
        """
        gradientParamBatch = []
        for batch in range(len(maps)):
            clonedMaps = [Variable(pmap.clone(), requires_grad=True) for pmap in maps[batch]]
            gradientParamBatch.append(self.signalModel.gradients(signal[batch], self.noiseModel, clonedMaps))
        paramGrad = torch.stack(gradientParamBatch)
        paramGrad[torch.isnan(paramGrad)] = 0
        paramGrad[torch.isinf(paramGrad)] = 0
        return paramGrad

    def setOpts(self, option):
        """
            Set custom parameters to model
        """
        self.config_parameters[option.key()] = option.value()
    
    @ProgressBarWrap
    def update_bar(self, loss, args):
        return -1

    def forward(self, signal, args):
        self.args = args
        self.args.number_of_iterations = self.config_parameters["rnn_iterations"]
        
        paramMaps = self.signalModel.initializeParameters(signal, self.args)
        hidden = self.__initHidden__(signal, self.args)
        estimates = []
        for _ in range(self.config_parameters["rnn_iterations"]):
            paramGrad = self.__getGradients__(signal, paramMaps)
            input = torch.cat([paramMaps, paramGrad], 1)
            dx, hidden = self.rnn.forward(input, hidden, self.args)
            paramMaps = torch.abs(paramMaps + dx)

            if self.args.stateName == "training":
                estimates.append(paramMaps)

            self.update_bar('-', self.args)
            
        if args.stateName == "validation" or args.stateName == "testing":
            estimates = paramMaps[0]
            
        return estimates




