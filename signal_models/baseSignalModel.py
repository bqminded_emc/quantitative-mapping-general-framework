from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from abc import ABC, abstractmethod

class BaseModel(ABC):
    
    def __init__(self):
        super().__init__()

    @abstractmethod
    def forwardModel(self):
        raise NotImplementedError("Forward_model not implemented")

    @abstractmethod
    def generateWeightedImages(self):
        raise NotImplementedError("Generate_weighted_sequence not implemented")

    @abstractmethod
    def gradients(self):
        """ Either defined explicitly based on signal model or instanciated with AutoDiff"""
        raise NotImplementedError("Gradients not implemented")

    @abstractmethod
    def initializeParameters(self):
        """ Initialize parameters according to the signal model"""
        raise NotImplementedError("Parameter initialization method not implemented")