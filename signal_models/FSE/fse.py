from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")

from ..baseSignalModel import BaseModel
import torch


class FSE_T2(BaseModel):
    """
        Class implementing the forward model of a Fast Spin Echo (FSE) based acquisition.
        Contains methods:
            - setTau: set inversion times of acquisition
            - forwardModel: Defines the signal model
            - generateWeightedImages: wrapper to generate N weighted images from A and T2 maps
            - gradients: Uses autograd to automatically compute gradients w.r.t. the likelihood function
            - initializeParameters: Initialises all tissue parameters 
    """
    def __init__(self, args):
        super(FSE_T2, self).__init__()
        self.args = args
        if args.tau:
            self.tau = torch.div(torch.Tensor(args.tau),1000)

    def setTau(self, tau):
        self.tau = tau

    def forwardModel(self, tau, kappa):
        return torch.abs(torch.abs(kappa[0]) * torch.exp(torch.div(tau*(-1), torch.abs(kappa[1]))))

    def generateWeightedImages(self, kappa):
        weightedImages = []
        for tau_ in self.tau:
            w_image = self.forwardModel(tau_, kappa).view(kappa[0].shape)
            weightedImages.append(w_image)
        return torch.stack(weightedImages)

    def gradients(self, *args):
        if len(args)>3:
            signal = args[0]
            lossCrit = args[1]
            kappa = args[2]
            sigma = args[3]
        else:
            signal = args[0]
            lossCrit = args[1]
            kappa = args[2]
            sigma = 1

        wImages = self.generateWeightedImages(kappa)
        loss = lossCrit.logLikelihood(signal, wImages, sigma)
        # print("Loss: {}".format(loss))
        loss.backward()
        param_map_gradient = ([param_map.grad.view(signal[0].size()) for param_map in kappa])
        return torch.stack(param_map_gradient)

    def initializeParameters(self, signal, args):
        mipSignal, _ = torch.max(signal[:], 1)

        if not args.inferenceModel == 'MLE':
            ro_map = torch.autograd.Variable(mipSignal.to(device=args.device), requires_grad=True)
            t2_map = torch.autograd.Variable(torch.ones_like(ro_map).to(device=args.device), requires_grad=True)
            permute_list = [1, 0] + [i+2 for i in range(ro_map.dim()-1)]
            initialized_variables = torch.stack([ro_map, t2_map]).permute(permute_list)
        else:
            ro_map = torch.autograd.Variable(mipSignal[0].to(device=args.device), requires_grad=True)
            t2_map = torch.autograd.Variable(torch.ones_like(ro_map).to(device=args.device), requires_grad=True)
            initialized_variables = [ro_map, t2_map]
        
        return initialized_variables







