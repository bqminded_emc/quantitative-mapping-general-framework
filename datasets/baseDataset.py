from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, '../')
from abc import ABC, abstractmethod


class BaseDataset(ABC):
    def __init__(self):
        super().__init__()
        
    @abstractmethod
    def index_files(self):
        raise NotImplementedError("index_files of dataset not implemented")

    @abstractmethod
    def get_length(self):
        raise NotImplementedError("get_length of dataset not implemented")

    @abstractmethod
    def set_data_path(self):
        raise NotImplementedError("set_data_path not implemented")

    @abstractmethod
    def get_training_label(self):
        raise NotImplementedError("get_training_label not implemented")

    @abstractmethod
    def get_training_signal(self):
        raise NotImplementedError("get_training_signal not implemented")
