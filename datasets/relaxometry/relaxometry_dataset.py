from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")

from ..baseDataset import BaseDataset
import datasets.utilities as datasetUtilities
import numpy as np
import os
import pickle
import torch

class RelaxometryDataset(BaseDataset):
    def __init__(self, args):
        self.args = args
        self.data = []
        self.idx_control = -1
        if args.sigmaNoise:
            self.sigma = args.sigmaNoise
        else:
            self.sigma = 1.0 #TODO: Make it random selection - Create iterator over random values for SNR

    def set_data_path(self, path):
        self.path = path

    def load_folder(self):
        for file in self.fileName:
            with open(os.path.join(self.path, file), 'rb') as f:
                data_ = pickle.load(f)
                self.data.append(data_)

    def index_files(self):
        extension = ".rawb" if self.args.useSimulatedData else ".pkl"
        self.fileName = sorted([f for f in os.listdir(self.path) if (os.path.isfile(os.path.join(self.path, f)) and (extension in f))])
        if self.args.preLoadData and not self.args.useSimulatedData:
            self.load_folder()
        elif not self.args.preLoadData and not self.args.useSimulatedData:
            self.load_file(0)
        elif self.args.useSimulatedData:
            self.get_anatomical_maps()

    def load_file(self, idx):
        file = self.fileName[idx]
        with open(os.path.join(self.path, file), 'rb') as f:
            data_ = pickle.load(f)
            self.data = [data_]
            self.idx_control = idx

    def get_anatomical_maps(self):
        # fileList = sorted([f for f in os.listdir(self.path) if (os.path.isfile(os.path.join(self.path, f)) and (".rawb" in f))])
        # self.fileName = fileList
        # print(self.fileName)
        for file in self.fileName:
            raw_data = np.fromfile(os.path.join(self.path, file), dtype='int8', sep="")
            raw_data = np.reshape(raw_data, (362, 434, 362))
            self.data.append(raw_data)   

    def set_parameter_values(self):
        if list(self.args.signalModel.keys())[0] == 'T1':
            tp = {"csf": 3.5, "gm": 1.4, "wm": 0.78, "fat": 0.42, 
            "muscle": 1.2, "muscle_skin": 1.23, "skull": 0.4, "vessels": 1.98, 
            "connect": 0.9, "dura": 0.9, "bone_marrow": 0.58}
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                "muscle": 0.7, "muscle_skin": 0.7, "skull": 0.9, "vessels": 1.0,
                "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}
            
        elif list(self.args.signalModel.keys())[0] == 'T2':
            tp = {"csf": 2, "gm": 0.11, "wm": 0.08, "fat": 0.07, 
            "muscle": 0.05, "muscle_skin": 0.05, "skull": 0.03, "vessels": 0.275, 
            "connect": 0.08, "dura": 0.07, "bone_marrow": 0.05}
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                "muscle": 0.7, "muscle_skin": 0.7, "skull": 0.9, "vessels": 1.0,
                "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}
        self.parameters = [tp, pd]     

    def generate_simulated_training_label(self, idx):
        data_ = self.data[idx]
        self.args.pngr = datasetUtilities.get_rand_seed(self.args.useRandomSeed)
        if self.args.usePatches:
            data_ = datasetUtilities.get_patch(data_, self.args)

        slice_index = int(self.args.pngr.uniform(10, int(data_.shape[-1])-10))
        data_ = data_[..., slice_index]

        qMap = np.zeros_like(data_, dtype=float)
        pdMap = np.zeros_like(data_, dtype=float)
        mask = np.zeros_like(data_, dtype=float)
        mask[data_ > 1] = 1

        self.set_parameter_values()
        relaxation_param = list(self.parameters[0].items())
        proton_density = list(self.parameters[1].items())

        if len(np.shape(data_)) > 2:
            for p in range(len(data_)):
                self.args.pngr = datasetUtilities.get_rand_seed(self.args.useRandomSeed)
                for ind in np.unique(data_):
                    if ind>0:
                        qMap[p, data_[p] == ind] = np.abs(self.args.pngr.normal(relaxation_param[ind-1][1], 0.3))
                        pdMap[p, data_[p] == ind] = np.abs(self.args.pngr.normal(proton_density[ind-1][1], 0.3))+0.1
        else:
            for ind in np.unique(data_):
                if ind > 0 :    
                    qMap[data_ == ind] = np.abs(self.args.pngr.normal(relaxation_param[ind-1][1], 0.3))
                    pdMap[data_ == ind] = np.abs(self.args.pngr.normal(proton_density[ind-1][1], 0.3))+0.1
        
        qMap = np.stack(qMap)
        pdMap = np.stack(pdMap)

        if list(self.args.signalModel.keys())[0] == 'T1':
            bMap = []
            for pi, p_map in enumerate(pdMap):
                self.args.pngr = datasetUtilities.get_rand_seed(self.args.useRandomSeed)
                abs_bMap = np.abs(self.args.pngr.normal(2.0, 0.0, np.shape(p_map)))
                bMap_ = (2 - np.abs(abs_bMap - 2))*mask[pi]
                bMap.append(bMap_)
            bMap = np.stack(bMap)
            kappa = [pdMap, bMap, qMap]
        elif list(self.args.signalModel.keys())[0] == 'T2':
            kappa = [pdMap, qMap]
        
        kappa = datasetUtilities.apply_gt_noise(kappa, self.args.pngr, self.args)
        kappa = datasetUtilities.smooth_maps(kappa, self.args)

        if self.args.simulateArtefacts:
            kappa = datasetUtilities.add_artefacts(kappa)

        self.training_label = torch.from_numpy(np.abs(kappa)).type(torch.FloatTensor)
        self.mask = torch.from_numpy(mask).type(torch.FloatTensor)

    def generate_simulated_training_signal(self):
        weightedSeries = []
        for label_patch in self.training_label:
            weighted_images = list(self.args.signalModel.values())[0].generateWeightedImages(label_patch)
            weighted_images_noisy = list(self.args.likelihood.values())[0].applyNoise(weighted_images, self.sigma, self.args.pngr)
            weightedSeries.append(weighted_images_noisy)
        self.training_signal = torch.stack(weightedSeries).type(torch.FloatTensor)

    def get_existing_data(self, idx):
        if not self.args.preLoadData:
            data_ = self.data[idx - self.idx_control]
        else:
            data_ = self.data[idx]
            
        self.training_signal = torch.Tensor(data_['weighted_series']).type(torch.FloatTensor)

        if 'label' in data_:
            self.training_label = torch.Tensor(data_['label']).type(torch.FloatTensor)
        else:
            self.training_label = []

        if 'mask' in data_:
            self.mask = torch.Tensor(data_['mask']).type(torch.FloatTensor)
        else:
            self.mask = []

        if self.training_signal.dim() > 3:
            self.training_signal = self.training_signal[...,0]
        list(self.args.signalModel.values())[0].setTau(torch.Tensor(data_['echo_times']).type(torch.FloatTensor))

    def get_training_label(self, idx):
        if self.args.useSimulatedData:
            self.generate_simulated_training_label(idx)
        else:
            if not self.args.preLoadData:
                self.load_file(idx)
            self.get_existing_data(idx)
        return self.training_label, self.mask #[self.mask, self.fileName[idx]]

    def get_training_signal(self, *local_args):
        if self.args.useSimulatedData:
            self.generate_simulated_training_signal()
        return self.training_signal

    def get_length(self):
        return len(self.fileName)
