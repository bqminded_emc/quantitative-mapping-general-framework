from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")

from ..baseLL import BaseLikelihood
import torch
import math

class Gaussian(BaseLikelihood):
    """
        Class for the Gaussian PDF.
        Methods:
            - logLikelihood
                inputs: signal (measured signal), mu (simulated signal) and sigma (SD of the noise)
                outputs: data consistency loss
            - applyNoise 
                inputs: a signal and sigma
                outputs: Noisy signal corrupted by additive gaussian noise
    """
    
    def __init__(self):
        super(Gaussian, self).__init__()

    def logLikelihood(self, *args):
        n_elem = torch.numel(args[0])
        # return -(n_elem/2)*math.log(2*math.pi) - (n_elem)*math.log(args[2]) - (1/(2*args[2]**2))*torch.sum((args[0] - args[1])**2)
        return torch.sum((args[0] - args[1])**2)

    def applyNoise(self, signal, sigma, pngr):
        signal += torch.from_numpy(pngr.normal(0.0, sigma, signal.size()))
        return signal

    