from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
sys.path.insert(0, "../")
from abc import ABC, abstractmethod

class BaseLikelihood(ABC):
    """
        Abstract class for implementation of likelihood models.
        A derived class should implement the following methods:
            - logLikelihood: Method containing the log likelihood (or cost) function
            - applyNoise: Method implementing noise corruption for specific Probability Density Function.
    """

    def __init__(self):
        super().__init__()

    @abstractmethod
    def applyNoise(self):
        raise NotImplementedError("Apply Noise not implemented")

    @abstractmethod
    def logLikelihood(self):
        raise NotImplementedError("Likelihood Function not implemented")


        
